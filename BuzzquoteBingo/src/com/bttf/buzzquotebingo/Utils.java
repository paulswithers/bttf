package com.bttf.buzzquotebingo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Notification;

public class Utils {

	public static VaadinRequest getCurrentRequest() {
		VaadinRequest request = VaadinService.getCurrentRequest();
		if (request == null) {
			throw new IllegalStateException("No request bound to current thread");
		}
		return request;
	}

	public static String getSignerName() {
		return VaadinServlet.getCurrent().getServletContext().getInitParameter("org.openntf.crossworlds.appsignername");
	}

	public static String getBaseFolder() {
		return VaadinServlet.getCurrent().getServletContext().getInitParameter("bttf.basePath");
	}

	public static String getInstanceOU() {
		return VaadinServlet.getCurrent().getServletContext().getInitParameter("bttf.instanceOU");
	}

	public static void throwError(Logger log, Throwable t, String msg) {
		if (StringUtils.isEmpty(msg)) {
			msg = "Sorry, we've hit an error";
		}
		if (null != log) {
			log.log(Level.SEVERE, msg, log);
		}
		t.printStackTrace();
		Notification.show(msg, Notification.Type.ERROR_MESSAGE);
	}

	/**
	 * Converts a date to a standard system-wide format
	 *
	 * @param passedDate
	 *            java.util.Date date passed
	 * @return String formatted to dd/MM/yyyy
	 */
	public static String convertDate(Date passedDate) {
		final SimpleDateFormat DATE_FORMAT_DATE_ONLY = new SimpleDateFormat("dd/MM/yyyy");
		return DATE_FORMAT_DATE_ONLY.format(passedDate);
	}

	/**
	 * Converts a date-time to a standard system-wide format
	 *
	 * @param passedDate
	 *            java.util.Date date passed
	 * @return String formatted to dd/MM/yyyy hh:mm:ss
	 */
	public static String convertDateAndTime(Date passedDate) {
		final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		return DATE_FORMAT.format(passedDate);
	}

}