package com.bttf.buzzquotebingo.domino;

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.openntf.domino.ACL;
import org.openntf.domino.Database;
import org.openntf.domino.Database.DBOption;
import org.openntf.domino.DbDirectory;
import org.openntf.domino.Document;
import org.openntf.domino.Session;
import org.openntf.domino.ext.Session.Fixes;
import org.openntf.domino.utils.DominoUtils;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.authentication.CurrentUser;

public class GenericDatabaseUtils {

	private static Logger log = Logger.getLogger(CurrentUser.class.getName());

	public enum DatabaseOption {
		QUOTES("quotes.ntf"), CARDS("cards.ntf"), ADMIN("admin.ntf"), DEFAULT("default.ntf");

		private final String value_;

		private DatabaseOption(String fileName) {
			value_ = fileName;
		}

		public String getValue() {
			return value_;
		}

	}

	/**
	 * Gets the current user session
	 *
	 * @return Session current user's Session
	 */
	public static Session getUserSession() {
		final Session sess = Factory.getSession(SessionType.CURRENT);
		sess.setFixEnable(Fixes.APPEND_ITEM_VALUE, true);
		sess.setFixEnable(Fixes.DOC_UNID_NULLS, true);
		sess.setFixEnable(Fixes.FORCE_JAVA_DATES, true);
		sess.setFixEnable(Fixes.MIME_CONVERT, true);
		sess.setFixEnable(Fixes.ODA_NAMES, true);
		sess.setFixEnable(Fixes.REMOVE_ITEM, true);
		sess.setFixEnable(Fixes.REPLACE_ITEM_NULL, true);
		sess.setFixEnable(Fixes.VIEW_UPDATE_OFF, true);
		sess.setFixEnable(Fixes.VIEWENTRY_RETURN_CONSTANT_VALUES, true);
		return sess;
	}

	public static Session getSignerSession() {
		final Session sess = Factory.getSession(SessionType.SIGNER);
		sess.setFixEnable(Fixes.APPEND_ITEM_VALUE, true);
		sess.setFixEnable(Fixes.DOC_UNID_NULLS, true);
		sess.setFixEnable(Fixes.FORCE_JAVA_DATES, true);
		sess.setFixEnable(Fixes.MIME_CONVERT, true);
		sess.setFixEnable(Fixes.ODA_NAMES, true);
		sess.setFixEnable(Fixes.REMOVE_ITEM, true);
		sess.setFixEnable(Fixes.REPLACE_ITEM_NULL, true);
		sess.setFixEnable(Fixes.VIEW_UPDATE_OFF, true);
		sess.setFixEnable(Fixes.VIEWENTRY_RETURN_CONSTANT_VALUES, true);
		return sess;
	}

	public static Document getDocumentByNoteID_Or_UNID(DatabaseOption databaseOpt, String unid) {
		return getDocumentByNoteID_Or_UNID(getUserSession(), databaseOpt, unid);
	}

	public static Document getDocumentByNoteID_Or_UNID(Session session, DatabaseOption databaseOpt, String unid) {
		Document doc;
		doc = getDb(session, databaseOpt, false).getDocumentByUNID(unid);
		if (doc == null) {
			try {
				doc = getDb(databaseOpt).getDocumentByID(unid);
			} catch (final Throwable te) {
				// Just couldn't get doc
			}
		}
		return doc;
	}

	public static Database getDb(DatabaseOption fileName) {
		return getDb(getUserSession(), fileName, false);
	}

	public static Database getDb() {
		return getDb(getUserSession(), DatabaseOption.CARDS, false);
	}

	public static Database getDb(Session session, DatabaseOption fileName, boolean createDb) {
		Database retVal = null;
		try {
			retVal = session.getDatabase(session.getServerName(), Utils.getBaseFolder() + fileName.getValue(), false);
			if (createDb) {
				final DbDirectory dir = session.getDbDirectory(session.getServerName());
				retVal = dir.createDatabase(Utils.getBaseFolder() + fileName.getValue(), true);
				retVal.setListInDbCatalog(false);
				retVal.setTitle(Utils.getInstanceOU() + fileName.name());
				retVal.setOption(DBOption.COMPRESSDOCUMENTS, true);
				retVal.setOption(DBOption.LZ1, true);
				retVal.setOption(DBOption.LZCOMPRESSION, true);
				retVal.setOption(DBOption.MAINTAINLASTACCESSED, false);
				retVal.setOption(DBOption.NORESPONSEINFO, true);
				retVal.setOption(DBOption.NOTRANSACTIONLOGGING, true);
				retVal.setOption(DBOption.NOUNREAD, true);
				retVal.setOption(DBOption.OPTIMIZATION, true);
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "Error getting database - " + fileName.getValue());
		}
		return retVal;
	}

	public static void setAcl(ACL acl, DatabaseOption fileType, String currentUser) {
		try {
			acl.createACLEntry("LocalDomainServers", ACL.Level.MANAGER);
			acl.createACLEntry("LocalDomainAdmins", ACL.Level.MANAGER);
			acl.createACLEntry("Anonymous", ACL.Level.NOACCESS);
			acl.createACLEntry(currentUser, ACL.Level.EDITOR);
			acl.getEntry("-Default-").setLevel(ACL.LEVEL_NOACCESS);
			if (fileType.equals(DatabaseOption.ADMIN)) {
				acl.createACLEntry("*/" + Utils.getInstanceOU() + "/O=Intec", ACL.Level.NOACCESS);
			} else if (fileType.equals(DatabaseOption.QUOTES)) {
				acl.createACLEntry("*/" + Utils.getInstanceOU() + "/O=Intec", ACL.Level.EDITOR);
			} else if (fileType.equals(DatabaseOption.CARDS)) {
				acl.createACLEntry("*/" + Utils.getInstanceOU() + "/O=Intec", ACL.Level.EDITOR);
			} else if (fileType.equals(DatabaseOption.DEFAULT)) {
				acl.createACLEntry("*/" + Utils.getInstanceOU() + "/O=Intec", ACL.Level.READER);
			}
			acl.save();
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
	}

	public static boolean dbExists() {
		if (null == getDb(getSignerSession(), DatabaseOption.ADMIN, false)) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean initialiseDbs(String userEmail) {
		try {
			final String currentUser = userEmail + "/BTTF_IBMConnect2015Admin/Intec";
			final Database adminDb = getDb(getSignerSession(), DatabaseOption.ADMIN, true);
			setAcl(adminDb.getACL(), DatabaseOption.ADMIN, currentUser);
			final Database quoteDb = getDb(getSignerSession(), DatabaseOption.QUOTES, true);
			setAcl(quoteDb.getACL(), DatabaseOption.QUOTES, currentUser);
			final Database cardsDb = getDb(getSignerSession(), DatabaseOption.CARDS, true);
			setAcl(cardsDb.getACL(), DatabaseOption.CARDS, currentUser);
			final Database defaultDb = getDb(getSignerSession(), DatabaseOption.DEFAULT, true);
			setAcl(defaultDb.getACL(), DatabaseOption.DEFAULT, currentUser);
			final String password = RandomStringUtils.random(8, true, true);
			final Document doc = adminDb.createDocument();
			doc.put("password", password);
			doc.put("author", userEmail);
			doc.setUniversalID(DominoUtils.toUnid(userEmail));
			doc.save();
			MainUI.get().setPassword(password);
			return true;
		} catch (final Exception e) {
			Utils.throwError(log, e, "Error creating databases and admin profile: " + e.getMessage());
			return false;
		}
	}

	public static boolean adminProfileExists(String userName) {
		try {
			final Document adminDoc = getAdminProfile(userName);
			if (null == adminDoc) {
				return false;
			} else {
				return true;
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
			return false;
		}
	}

	public static String getAdminPassword(String userName) {
		final Document adminDoc = getAdminProfile(userName);
		return adminDoc.getItemValueString("password");
	}

	private static Document getAdminProfile(String userName) {
		final Database quotesDb = getDb(GenericDatabaseUtils.getSignerSession(), DatabaseOption.ADMIN, false);
		final Document adminDoc = quotesDb.getDocumentByUNID(DominoUtils.toUnid(userName));
		return adminDoc;
	}

}
