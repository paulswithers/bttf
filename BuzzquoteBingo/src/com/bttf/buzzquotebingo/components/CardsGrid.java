package com.bttf.buzzquotebingo.components;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.domino.CardBean;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.views.CardCheckView;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import com.vaadin.ui.renderers.HtmlRenderer;

public class CardsGrid extends Grid {
	private static final long serialVersionUID = 1L;
	private static CardsGrid INSTANCE;

	public CardsGrid() {
		setSizeFull();

		setSelectionMode(SelectionMode.SINGLE);

		final BeanItemContainer<CardBean> container = new BeanItemContainer<CardBean>(CardBean.class);
		final GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(container);
		gpc.addGeneratedProperty("Open", new PropertyValueGenerator<String>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "Open";
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		setContainerDataSource(gpc);
		setColumnOrder("name", "key", "card", "quoteObjects", "quotes", "usedQuotes", "heardQuotes", "winner");
		getColumn("key").setHidden(true);
		getColumn("card").setHidden(true);
		getColumn("quoteObjects").setHidden(true);
		getColumn("quotes").setHidden(true);
		final Column winner = getColumn("winner");
		winner.setRenderer(new HtmlRenderer(), new StringToBooleanConverter(FontAwesome.STAR.getHtml(), FontAwesome.FROWN_O.getHtml()));
		final Column name = getColumn("name");
		name.setLastFrozenColumn();

		final RendererClickListener listener = new RendererClickListener() {

			@Override
			public void click(RendererClickEvent event) {
				select(event.getItemId());
				final CardBean card = (CardBean) event.getItemId();
				MainUI.get().getNavigator().navigateTo(CardCheckView.VIEW_NAME + "/" + card.getKey());
			}
		};
		final ButtonRenderer openButton = new ButtonRenderer(listener);

		getColumn("Open").setRenderer(openButton);

		// Create a header row to hold column filters
		final HeaderRow filterRow = appendHeaderRow();

		// Set up a filter for all columns
		for (final Object pid : getContainerDataSource().getContainerPropertyIds()) {
			if (!pid.equals("Open")) {
				final HeaderCell cell = filterRow.getCell(pid);

				// Have an input field to use for filter
				final TextField filterField = new TextField();
				filterField.setWidth(100, Unit.PERCENTAGE);

				// Update filter When the filter input is changed
				filterField.addTextChangeListener(new TextChangeListener() {
					@Override
					public void textChange(TextChangeEvent change) {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);

						// (Re)create the filter if necessary
						if (!change.getText().isEmpty()) {
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
						}
					}
				});
				cell.setComponent(filterField);
			}
		}

		INSTANCE = this;

	}

	public static CardsGrid getInstance() {
		return INSTANCE;
	}

	private GeneratedPropertyContainer getContainer() {
		return (GeneratedPropertyContainer) super.getContainerDataSource();
	}

	@Override
	public CardBean getSelectedRow() throws IllegalStateException {
		return (CardBean) super.getSelectedRow();
	}

	public void loadCards() {
		getContainer().removeAllItems();
		((BeanItemContainer<CardBean>) getContainer().getWrappedContainer()).addAll(BTTF_Graph.getInstance().loadCardBeans());
	}

	public void addBean(CardBean newQuote) {
		final BeanItemContainer<CardBean> container = ((BeanItemContainer<CardBean>) getContainer().getWrappedContainer());
		final CardBean oldQuote = getSelectedRow();
		if (null != oldQuote && oldQuote.getKey().equals(newQuote.getKey())) {
			// Do stuff
		} else {
			container.addBean(newQuote);
		}
	}
}
