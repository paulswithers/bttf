package com.bttf.buzzquotebingo.components;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.SectionName;
import com.bttf.buzzquotebingo.views.ManageQuotesView;
import com.bttf.buzzquotebingo.views.MarkQuotesSaidView;
import com.bttf.buzzquotebingo.views.QuoteView;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.data.util.filter.Not;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import com.vaadin.ui.renderers.HtmlRenderer;

public class QuotesGrid extends Grid {
	private static final long serialVersionUID = 1L;
	private static QuotesGrid INSTANCE;

	public QuotesGrid() {

	}

	public QuotesGrid(String viewType) {
		setSizeFull();

		setSelectionMode(SelectionMode.SINGLE);

		final BeanItemContainer<QuoteBean> container = new BeanItemContainer<QuoteBean>(QuoteBean.class);
		final GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(container);
		if (viewType.equals(ManageQuotesView.VIEW_NAME)) {
			gpc.addGeneratedProperty("Open", new PropertyValueGenerator<String>() {
				private static final long serialVersionUID = 1L;

				@Override
				public String getValue(Item item, Object itemId, Object propertyId) {
					return "Open";
				}

				@Override
				public Class<String> getType() {
					return String.class;
				}
			});
		}

		setContainerDataSource(gpc);
		if (viewType.equals(MarkQuotesSaidView.VIEW_NAME)) {
			setColumnOrder("section", "summary", "quote", "film", "whoBy", "whoTo", "saidByPresenter");
			getColumn("whoBy").setHidden(true);
			getColumn("whoTo").setHidden(true);
			getColumn("film").setHidden(true);
			final Column section = getColumn("section");
			section.setWidth(50);
			final Column quote = getColumn("quote");
			quote.setMaximumWidth(700);
			final Column said = getColumn("saidByPresenter");
			said.setHeaderCaption("Said");
			said.setWidth(50);
			said.setRenderer(new HtmlRenderer(), new StringToBooleanConverter(FontAwesome.THUMBS_O_UP.getHtml(), FontAwesome.THUMBS_DOWN.getHtml()));
		}
		if (viewType.equals(ManageQuotesView.VIEW_NAME)) {
			setColumnOrder("summary", "quote", "film", "whoBy", "whoTo", "section", "saidByPresenter");
			getColumn("saidByPresenter").setHidden(true);
			final Column quote = getColumn("quote");
			quote.setMaximumWidth(200);
			quote.setLastFrozenColumn();
		}
		getColumn("summary").setHidden(true);
		final Column whoByCol = getColumn("whoBy");
		whoByCol.setHeaderCaption("Quoted By");
		final Column whoToCol = getColumn("whoTo");
		whoToCol.setHeaderCaption("Said To");

		if (viewType.equals(ManageQuotesView.VIEW_NAME)) {
			final RendererClickListener listener = new RendererClickListener() {

				@Override
				public void click(RendererClickEvent event) {
					select(event.getItemId());
					final QuoteBean quote = (QuoteBean) event.getItemId();
					MainUI.get().getNavigator().navigateTo(QuoteView.VIEW_NAME + "/" + quote.getSummary());
				}
			};
			final ButtonRenderer openButton = new ButtonRenderer(listener);
			getColumn("Open").setRenderer(openButton);
		}

		// Create a header row to hold column filters
		final HeaderRow filterRow = appendHeaderRow();

		// Set up a filter for all columns
		for (final Object pid : getContainerDataSource().getContainerPropertyIds()) {
			if (!pid.equals("Open")) {
				final HeaderCell cell = filterRow.getCell(pid);

				// Have an input field to use for filter
				final TextField filterField = new TextField();
				filterField.setWidth(100, Unit.PERCENTAGE);

				// Update filter When the filter input is changed
				filterField.addTextChangeListener(new TextChangeListener() {
					@Override
					public void textChange(TextChangeEvent change) {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);

						// (Re)create the filter if necessary
						if (!change.getText().isEmpty()) {
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
						}
					}
				});
				cell.setComponent(filterField);
			}
		}

		INSTANCE = this;

	}

	public static QuotesGrid getInstance() {
		return INSTANCE;
	}

	private GeneratedPropertyContainer getContainer() {
		return (GeneratedPropertyContainer) super.getContainerDataSource();
	}

	/**
	 * Filter the grid based on a search string that is searched for in the
	 * product name, availability and category columns.
	 *
	 * @param filterString
	 *            string to look for
	 */
	public void setFilter(String filterString) {
		getContainer().removeAllContainerFilters();
		if (filterString.length() > 0) {
			final SimpleStringFilter quoteFilter = new SimpleStringFilter("quote", filterString, true, false);
			getContainer().addContainerFilter(new Or(quoteFilter));
		}
	}

	public void setUsedFilter() {
		getContainer().removeAllContainerFilters();
		final SimpleStringFilter filter = new SimpleStringFilter("section", SectionName.NONE.getValue(), true, false);
		getContainer().addContainerFilter(new Not(filter));
	}

	@Override
	public QuoteBean getSelectedRow() throws IllegalStateException {
		return (QuoteBean) super.getSelectedRow();
	}

	public void loadQuotes() {
		getContainer().removeAllItems();
		((BeanItemContainer<QuoteBean>) getContainer().getWrappedContainer()).addAll(BTTF_Graph.getInstance().loadQuoteBeans());
	}

	public void removeBean(QuoteBean oldQuote) {
		final BeanItemContainer<QuoteBean> container = ((BeanItemContainer<QuoteBean>) getContainer().getWrappedContainer());
		final BeanItem<QuoteBean> item = container.getItem(oldQuote);
		if (null != item) {
			container.removeItem(item);
		}
	}

	public void addBean(QuoteBean newQuote) {
		final BeanItemContainer<QuoteBean> container = ((BeanItemContainer<QuoteBean>) getContainer().getWrappedContainer());
		final QuoteBean oldQuote = getSelectedRow();
		if (null != oldQuote && oldQuote.getSummary().equals(newQuote.getSummary())) {
			oldQuote.setQuote(newQuote.getQuote());
			oldQuote.setFilm(newQuote.getFilm());
			oldQuote.setWhoBy(newQuote.getWhoBy());
			oldQuote.setWhoTo(newQuote.getWhoTo());
			oldQuote.setSection(newQuote.getSection());
			oldQuote.setSaidByPresenter(newQuote.isSaidByPresenter());
		} else {
			container.addBean(newQuote);
		}
	}
}
