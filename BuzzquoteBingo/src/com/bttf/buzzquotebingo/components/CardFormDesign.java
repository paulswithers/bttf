package com.bttf.buzzquotebingo.components;

import java.util.logging.Logger;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class CardFormDesign extends Panel {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(CardFormDesign.class.getName());
	private VerticalLayout body;
	private HorizontalLayout header;
	private boolean narrow;

	public CardFormDesign() {

	}

	public VerticalLayout getBody() {
		if (null == body) {
			setBody(new VerticalLayout());
		}
		return body;
	}

	public void setBody(VerticalLayout body) {
		this.body = body;
	}

	public HorizontalLayout getHeader() {
		if (null == header) {
			setHeader();
		}
		return header;
	}

	public void setHeader() {
		final HorizontalLayout retVal_ = new HorizontalLayout();
		retVal_.setSizeFull();
		final Label label = new Label(getName());
		label.setStyleName(ValoTheme.LABEL_H2);
		final Button button = new Button();
		button.addStyleName(ValoTheme.BUTTON_ICON_ALIGN_RIGHT);
		if (MainUI.get().getAccessControl().isUserInRole("admin")) {
			button.setCaption("Check Card");
			button.setIcon(FontAwesome.CERTIFICATE);
			button.addStyleName(ValoTheme.BUTTON_DANGER);
			button.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					markCard();
				}

			});
		} else {
			button.setCaption("Toggle Layout");
			button.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			setLayoutButtonDetails(button, isNarrow());
			button.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					final Button parent = (Button) event.getComponent();
					resetLayout();
					if (parent.getIcon().equals(FontAwesome.ELLIPSIS_V)) {
						// i.e. currently wide
						loadOneColumnLayout();
						setLayoutButtonDetails(parent, true);
					} else {
						loadThreeColumnLayout();
						setLayoutButtonDetails(parent, false);
					}
				}

			});
		}
		retVal_.addComponents(label, button);
		retVal_.setExpandRatio(label, 1.0f);
		header = retVal_;
	}

	public boolean isNarrow() {
		return narrow;
	}

	public void setNarrow(boolean narrow) {
		this.narrow = narrow;
	}

	public void loadContents() {
		if (UI.getCurrent().getPage().getBrowserWindowWidth() < 400) {
			setNarrow(true);
			loadOneColumnLayout();
		} else {
			loadThreeColumnLayout();
		}
		addStyleName("card-screen");
		if (isNarrow()) {
			addStyleName("card-screen-mobile");
		}
		setSizeFull();
		setContent(getBody());
	}

	public void loadOneColumnLayout() {
		resetLayout();
	}

	public void loadThreeColumnLayout() {
		resetLayout();
	}

	public void resetLayout() {
		getBody().removeAllComponents();
		getBody().addComponent(getHeader());
		getBody().setComponentAlignment(getHeader(), Alignment.MIDDLE_CENTER);
	}

	public VerticalLayout createCell(final QuoteBean quote, Boolean heard, final int idex) {
		final VerticalLayout retVal_ = new VerticalLayout();
		try {
			retVal_.setSizeFull();
			retVal_.addStyleName("card-cell");
			final VerticalLayout labelDiv = new VerticalLayout();
			labelDiv.setStyleName("card-label-div");
			if (narrow) {
				labelDiv.addStyleName("card-label-div-mobile");
			}
			if (heard) {
				labelDiv.addStyleName("card-label-div-heard");
			}
			labelDiv.setSizeFull();
			final Label label = new Label(quote.getQuote());
			label.addStyleName("card-label");
			labelDiv.addComponent(label);
			labelDiv.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
			final HorizontalLayout buttonRow = new HorizontalLayout();
			buttonRow.setSizeFull();
			final Button more = new Button("More...");
			more.setStyleName(ValoTheme.BUTTON_PRIMARY);
			more.setIcon(FontAwesome.ELLIPSIS_H, "More...");
			more.setImmediate(true);
			more.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					final QuoteDetailsDialog dlg = new QuoteDetailsDialog();
					dlg.setHeight("10em");
					dlg.setWidth("30em");
					dlg.setCurrentQuote(quote);
					dlg.loadContent();
					// Add it to the root component
					UI.getCurrent().addWindow(dlg);
				}

			});

			final Button toggle = new Button();
			if (heard) {
				setHeardButtonDetails(toggle, true);
			} else {
				setHeardButtonDetails(toggle, false);
			}
			toggle.setImmediate(true);
			toggle.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					final Button parent = (Button) event.getComponent();
					if ("Heard".equals(parent.getCaption())) {
						toggleHeard(idex, true);
						setHeardButtonDetails(parent, true);
						labelDiv.addStyleName("card-label-div-heard");
					} else {
						toggleHeard(idex, false);
						setHeardButtonDetails(parent, false);
						labelDiv.removeStyleName("card-label-div-heard");
					}
				}

			});
			buttonRow.addComponents(more, toggle);
			buttonRow.setComponentAlignment(more, Alignment.BOTTOM_LEFT);
			buttonRow.setComponentAlignment(toggle, Alignment.TOP_RIGHT);
			retVal_.addComponents(labelDiv, buttonRow);
			retVal_.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);
			retVal_.setComponentAlignment(labelDiv, Alignment.TOP_CENTER);
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

	private void setHeardButtonDetails(Button button, boolean isHeard) {
		if (isHeard) {
			button.setCaption("Not Heard");
			button.setIcon(FontAwesome.TIMES_CIRCLE);
			button.setStyleName(ValoTheme.BUTTON_DANGER);
		} else {
			button.setCaption("Heard");
			button.setIcon(FontAwesome.CHECK_CIRCLE);
			button.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		}
	}

	protected void toggleHeard(int idex, boolean heard) {
		throw new UnsupportedOperationException("This method needs overriding");
	}

	protected void markCard() {
		throw new UnsupportedOperationException("This method needs overriding");
	}

	protected String getName() {
		throw new UnsupportedOperationException("This method needs overriding");
	}

	private void setLayoutButtonDetails(Button button, boolean isVertical) {
		if (isVertical) {
			button.setIcon(FontAwesome.ELLIPSIS_H);
		} else {
			button.setIcon(FontAwesome.ELLIPSIS_V);
		}
	}
}
