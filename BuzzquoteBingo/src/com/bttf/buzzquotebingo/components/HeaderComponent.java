package com.bttf.buzzquotebingo.components;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.MenuBar;

/**
 * @author Paul Withers<br/>
 *         <br/>
 *         Header area of the application
 */
public class HeaderComponent extends HorizontalLayout {
	private static final long serialVersionUID = 1L;
	private MenuBar menubar;

	/**
	 * Constructor, passing the Vaadin application page
	 *
	 * @param ui
	 *            MainUI application page
	 */
	public HeaderComponent() {
		setHeight("50px");
		setWidth(100, Unit.PERCENTAGE);
		setStyleName("header");

		final ThemeResource resource = new ThemeResource("img/intec-logo.png");
		final Image bannerImg = new Image();
		bannerImg.setAlternateText("Intec");
		bannerImg.setHeight("50px");
		bannerImg.setDescription("Intec Logo");
		bannerImg.setSource(resource);
		bannerImg.setWidth(null);
		bannerImg.setStyleName("bannerImg");

		final ThemeResource openntfResource = new ThemeResource("img/openntf_banner.jpg");
		final Image openntfImg = new Image();
		openntfImg.setAlternateText("OpenNTF");
		openntfImg.setHeight("50px");
		openntfImg.setDescription("OpenNTF Logo");
		openntfImg.setSource(openntfResource);
		openntfImg.setWidth(null);

		// final MenuItem userItem = menubar.addItem(getUserName(), null);
		// userItem.setStyleName("menuRight");

		addComponents(bannerImg, openntfImg);
		setComponentAlignment(openntfImg, Alignment.TOP_RIGHT);
	}

	/**
	 * Getter for menubar
	 *
	 * @return MenuBar allows external access to the menu bar created and added
	 *         to the header
	 */
	public MenuBar getMenubar() {
		return menubar;
	}

	/**
	 * Setter for menubar
	 *
	 * @param menubar
	 *            MenuBar being added to this header
	 */
	public void setMenubar(MenuBar menubar) {
		this.menubar = menubar;
	}

}
