package com.bttf.buzzquotebingo.util;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;

public class PageTitleUpdater implements ViewChangeListener {
	@Override
	public boolean beforeViewChange(ViewChangeEvent event) {
		return true;
	}

	@Override
	public void afterViewChange(ViewChangeEvent event) {

		final View view = event.getNewView();
		final ViewConfig viewConfig = view.getClass().getAnnotation(ViewConfig.class);

		if (viewConfig != null) {
			Page.getCurrent().setTitle(viewConfig.displayName());
		}

	}
}