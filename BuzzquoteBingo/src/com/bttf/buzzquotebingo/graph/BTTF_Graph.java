package com.bttf.buzzquotebingo.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.openntf.domino.graph2.builtin.DVertexFrameComparator;
import org.openntf.domino.graph2.impl.DConfiguration;
import org.openntf.domino.graph2.impl.DElementStore;
import org.openntf.domino.graph2.impl.DFramedGraphFactory;
import org.openntf.domino.graph2.impl.DFramedTransactionalGraph;
import org.openntf.domino.graph2.impl.DGraph;

import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.domino.CardBean;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.google.common.collect.Ordering;
import com.ibm.commons.util.StringUtil;

public class BTTF_Graph {
	public static String QUOTE_PATH;
	public static String CARDS_PATH;
	public static String DEFAULT_PATH;
	private static final Logger log = Logger.getLogger(BTTF_Graph.class.getName());
	private static BTTF_Graph INSTANCE;

	private DFramedTransactionalGraph<DGraph> framedGraph_;

	public BTTF_Graph() {
		final String baseFolder = Utils.getBaseFolder();
		QUOTE_PATH = baseFolder + "quotes.ntf";
		CARDS_PATH = baseFolder + "cards.ntf";
		DEFAULT_PATH = baseFolder + "default.ntf";
		initialise();
	}

	public void initialise() {
		final DElementStore quoteStore = new DElementStore();
		quoteStore.setStoreKey(QUOTE_PATH);
		quoteStore.addType(Quote.class);
		quoteStore.addType(FilmCharacter.class);
		quoteStore.addType(Section.class);
		final DElementStore cardStore = new DElementStore();
		cardStore.setStoreKey(CARDS_PATH);
		cardStore.addType(Card.class);
		final DElementStore defaultStore = new DElementStore();
		defaultStore.setStoreKey(DEFAULT_PATH);

		final DConfiguration config = new DConfiguration();
		final DGraph graph = new DGraph(config);
		config.addElementStore(quoteStore);
		config.addElementStore(cardStore);
		config.addElementStore(defaultStore);
		config.setDefaultElementStore(defaultStore.getStoreKey());

		final DFramedGraphFactory factory = new DFramedGraphFactory(config);
		framedGraph_ = (DFramedTransactionalGraph) factory.create(graph);
		BTTF_Initializer.createSections(framedGraph_);
		framedGraph_.commit();
	}

	public DFramedTransactionalGraph<DGraph> getFramedGraph() {
		return framedGraph_;
	}

	public static BTTF_Graph getInstance() {
		if (null == INSTANCE) {
			INSTANCE = new BTTF_Graph();
		}
		return INSTANCE;
	}

	public synchronized Iterable<Section> getSections() {
		return getFramedGraph().getVertices(null, null, Section.class);
	}

	public List<Quote> getQuotesSortedByProperty(String property) {
		List<Quote> retVal_ = null;
		try {
			final Iterable<Quote> quotes = framedGraph_.getVertices(null, null, Quote.class);
			if (StringUtil.isEmpty(property)) {
				property = "summary";
			}
			final Ordering ord = Ordering.from(new DVertexFrameComparator(property));
			retVal_ = ord.sortedCopy(quotes);
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

	public List<QuoteBean> loadQuoteBeans() {
		final List<QuoteBean> retVal_ = new ArrayList<QuoteBean>();
		try {
			final Iterable<Quote> quotes = framedGraph_.getVertices(null, null, Quote.class);
			final Iterator<Quote> it = quotes.iterator();
			int i = 0;
			while (it.hasNext()) {
				final Quote nextQuote = it.next();
				final QuoteBean quote = new QuoteBean(nextQuote);
				retVal_.add(quote);
				if (i > 200) {
					break;
				}
				i++;
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

	public List<QuoteBean> loadQuoteBeansForSection(SectionName section) {
		final List<QuoteBean> retVal_ = new ArrayList<QuoteBean>();
		try {
			final Section sec = BTTF_Graph.getInstance().getFramedGraph().addVertex(section.getValue(), Section.class);
			final Iterable<Quote> quotes = sec.getQuotes();
			final Iterator<Quote> it = quotes.iterator();
			int i = 0;
			while (it.hasNext()) {
				final Quote nextQuote = it.next();
				final QuoteBean quote = new QuoteBean(nextQuote);
				retVal_.add(quote);
				if (i > 200) {
					break;
				}
				i++;
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

	public List<CardBean> loadCardBeans() {
		final List<CardBean> retVal_ = new ArrayList<CardBean>();
		try {
			final Iterable<Card> cards = framedGraph_.getVertices(null, null, Card.class);
			final Iterator<Card> it = cards.iterator();
			int i = 0;
			while (it.hasNext()) {
				final Card nextCard = it.next();
				final CardBean card = new CardBean(nextCard);
				retVal_.add(card);
				if (i > 200) {
					break;
				}
				i++;
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

}
