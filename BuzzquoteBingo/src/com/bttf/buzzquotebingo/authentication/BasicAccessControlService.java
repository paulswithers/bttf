package com.bttf.buzzquotebingo.authentication;

import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;

public class BasicAccessControlService implements AccessControl {
	private final static Logger log = Logger.getLogger(BasicAccessControlService.class.getName());

	@Override
	public boolean signIn(String userEmail) {

		log.info("signIn " + userEmail);

		if (StringUtils.isEmpty(userEmail)) {
			return false;
		}

		CurrentUser.set(userEmail);

		log.info("clearing session identity");
		Factory.getSession(SessionType.CURRENT).clearIdentity();

		return true;
	}

	@Override
	public boolean isUserSignedIn() {
		return !CurrentUser.get().isEmpty();
	}

	@Override
	public boolean isUserInRole(String role) {
		if ("admin".equals(role)) {
			return getPrincipalName().equals("admin");
		}

		// All users are in all non-admin roles
		return true;
	}

	@Override
	public String getPrincipalName() {
		return CurrentUser.get();
	}

	@Override
	public void logout() {
		CurrentUser.set(null);
	}
}
