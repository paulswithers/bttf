package com.bttf.buzzquotebingo;

import javax.servlet.annotation.WebServlet;

import org.apache.commons.lang3.StringUtils;

import com.bttf.buzzquotebingo.authentication.CurrentUser;
import com.bttf.buzzquotebingo.components.CardForm;
import com.bttf.buzzquotebingo.components.HeaderComponent;
import com.bttf.buzzquotebingo.domino.BttfAccessControlService;
import com.bttf.buzzquotebingo.domino.CardBean;
import com.bttf.buzzquotebingo.views.AdminLoginScreen;
import com.bttf.buzzquotebingo.views.AdminScreen;
import com.bttf.buzzquotebingo.views.LoginScreen;
import com.bttf.buzzquotebingo.views.LoginScreen.LoginListener;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@Viewport("user-scalable=no,initial-scale=1.0")
@Theme("buzzquotebingo")
@Widgetset("com.bttf.buzzquotebingo.widgetset.BuzzquotebingoWidgetset")
@PreserveOnRefresh
public class MainUI extends UI {
	private static final long serialVersionUID = 1L;
	private HeaderComponent header;
	private String password;
	private String commonName;
	private String email;
	private BttfAccessControlService accessControl = new BttfAccessControlService();

	@WebServlet(urlPatterns = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MainUI.class, widgetset = "com.bttf.buzzquotebingo.widgetset.BuzzquotebingoWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	/**
	 * Constructor
	 */
	public MainUI() {

	}

	/**
	 * Overloaded constructor
	 *
	 * @param content
	 */
	public MainUI(Component content) {
		super(content);
	}

	/**
	 * Gets this Vaadin UI object, casting as a MainUI
	 *
	 * @return MainUI this object
	 */
	public static MainUI get() {
		return (MainUI) UI.getCurrent();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BttfAccessControlService getAccessControl() {
		return accessControl;
	}

	public void setAccessControl(BttfAccessControlService accessControl) {
		this.accessControl = accessControl;
	}

	@Override
	protected void init(VaadinRequest request) {
		setRequest(request);

		Responsive.makeResponsive(this);
		setLocale(request.getLocale());

		getPage().setTitle("BTTF Buzzquote Bingo");

		header = new HeaderComponent();

		if (!getAccessControl().isUserSignedIn()) {
			setContent(new LoginScreen(getAccessControl(), new LoginListener() {
				@Override
				public void loginSuccessful() {
					System.out.println("Successful login");
					loadViews();
				}
			}));
		} else {
			loadViews();
		}

	}

	public void loadViews() {
		if (getAccessControl().isUserInRole("admin")) {
			if (StringUtils.isNotEmpty(getPassword())) {
				System.out.println("Password is " + getPassword());
				showAdminViews();
			} else {
				System.out.println("Adding admin login form");
				setContent(new AdminLoginScreen(accessControl, new AdminLoginScreen.LoginListener() {

					@Override
					public void loginSuccessful() {
						showAdminViews();
					}

					@Override
					public void loginAborted() {
						showCard();
					}

				}));
			}
		} else {
			System.out.println("Not admin");
			if (StringUtils.isEmpty(getEmail())) {
				setEmail(CurrentUser.get());
				setCommonName(CurrentUser.get());
			}
			showCard();
		}
	}

	public void showCard() {
		final CardBean cardBean = new CardBean(getEmail());
		setContent(new CardForm(cardBean));
	}

	public void showAdminViews() {
		setContent(new AdminScreen(this, getHeader()));
		getNavigator().navigateTo(getNavigator().getState());
	}

	public HeaderComponent getHeader() {
		return header;
	}

	private VaadinRequest request;

	public VaadinRequest getRequest() {
		return request;
	}

	public void setRequest(VaadinRequest request) {
		this.request = request;
	}

}