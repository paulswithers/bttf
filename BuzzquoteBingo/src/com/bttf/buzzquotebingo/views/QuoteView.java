package com.bttf.buzzquotebingo.views;

import org.apache.commons.lang3.StringUtils;
import org.openntf.domino.utils.Strings;

import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.components.QuoteForm;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.data.fieldgroup.DefaultFieldGroupFieldFactory;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@ViewConfig(uri = "Quote", displayName = "New Quote")
public class QuoteView extends CssLayout implements View {
	private static final long serialVersionUID = 1L;
	public static String VIEW_NAME = "Quote";
	private boolean loaded = false;
	private QuoteForm quoteForm;

	public QuoteView() {
		setSizeFull();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		try {
			final String[] params = event.getParameters().split("/");
			getQuoteForm().setThisQuote(new QuoteBean());
			getQuoteForm().getBeanFields().setBuffered(false);
			getQuoteForm().getBeanFields().setItemDataSource(getQuoteForm().getThisQuote());
			getQuoteForm().getBeanFields().setFieldFactory(DefaultFieldGroupFieldFactory.get());
			boolean isEdit = false;
			if (!Strings.isBlankString(params[0])) {
				final String key = params[0];
				getQuoteForm().getThisQuote().loadQuote(key);
				if (params.length > 1) {
					final String mode = params[1];
					if (StringUtils.equalsIgnoreCase("edit", mode)) {
						isEdit = true;
					}
				}
			} else {
				isEdit = true;
			}
			if (!isLoaded()) {
				if (setContent()) {
					setLoaded(true);
				}
			}
			getQuoteForm().getBeanFields().bindMemberFields(getQuoteForm());
			getQuoteForm().setMode(isEdit);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public boolean setContent() {
		boolean retVal_ = false;
		try {
			addComponent(getQuoteForm());
			retVal_ = true;
		} catch (final Exception e) {
			Utils.throwError(null, e, "");
		}
		return retVal_;
	}

	/**
	 * Shows an error message on the screen
	 *
	 * @param msg
	 *            String error to display
	 */
	public void showError(String msg) {
		Notification.show(msg, Type.ERROR_MESSAGE);
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public QuoteForm getQuoteForm() {
		if (null == quoteForm) {
			setQuoteForm();
		}
		return quoteForm;
	}

	public void setQuoteForm() {
		quoteForm = new QuoteForm();
	}

}
