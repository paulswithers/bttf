package com.bttf.buzzquotebingo.views;

import java.util.List;
import java.util.logging.Logger;

import org.openntf.domino.graph2.impl.DGraph;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.components.QuotesGrid;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.BTTF_Initializer;
import com.bttf.buzzquotebingo.graph.Quote;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.tinkerpop.frames.FramedTransactionalGraph;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@ViewConfig(uri = "", displayName = "Quotes")
public class ManageQuotesView extends CssLayout implements View {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ManageQuotesView.class.getName());
	public static String VIEW_NAME = "";
	private boolean loaded = false;
	private Button newQuote;
	private final QuotesGrid grid;

	public ManageQuotesView() {
		setSizeFull();

		grid = new QuotesGrid(VIEW_NAME);
		grid.addSelectionListener(new SelectionListener() {

			@Override
			public void select(SelectionEvent event) {
				// ManageQuotesView.openQuote(grid.getSelectedRow());
			}

		});
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (!isLoaded()) {
			if (setContent()) {
				setLoaded(true);
			}
		}

		if (grid.getContainerDataSource().size() > 9) {
			if (!AdminScreen.getInstance().isCardMenuLoaded()) {
				AdminScreen.getInstance().getMenu().addView(new CardView(), CardView.VIEW_NAME, "My Card", FontAwesome.TABLE);
				AdminScreen.getInstance().setCardMenuLoaded(true);
			}
		}
	}

	public boolean setContent() {
		boolean retVal_ = false;
		try {
			final HorizontalLayout topLayout = createTopBar();
			final VerticalLayout barAndGridLayout = new VerticalLayout();
			barAndGridLayout.addComponent(topLayout);
			barAndGridLayout.addComponent(grid);
			barAndGridLayout.setMargin(true);
			barAndGridLayout.setSpacing(true);
			barAndGridLayout.setSizeFull();
			barAndGridLayout.setExpandRatio(grid, 1);
			barAndGridLayout.setStyleName("crud-main-layout");

			addComponent(barAndGridLayout);

			grid.loadQuotes();
			retVal_ = true;
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

	public HorizontalLayout createTopBar() {

		newQuote = new Button("New Quote");
		newQuote.addStyleName(ValoTheme.BUTTON_PRIMARY);
		newQuote.setIcon(FontAwesome.PLUS_CIRCLE);
		newQuote.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				try {
					MainUI.get().getNavigator().navigateTo(QuoteView.VIEW_NAME);
				} catch (final Exception e) {
					// Bug means this throws an error
				}
			}

		});

		final HorizontalLayout topLayout = new HorizontalLayout();
		topLayout.setSpacing(true);
		topLayout.setWidth("100%");
		topLayout.addComponent(newQuote);
		topLayout.setComponentAlignment(newQuote, Alignment.MIDDLE_RIGHT);
		topLayout.setStyleName("top-bar");

		final List<Quote> quotes = BTTF_Graph.getInstance().getQuotesSortedByProperty("");

		if (quotes.isEmpty()) {
			final Button initialise = new Button("Initialise Data");
			initialise.addStyleName(ValoTheme.BUTTON_PRIMARY);
			initialise.setIcon(FontAwesome.DATABASE);
			initialise.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						final FramedTransactionalGraph<DGraph> framedGraph = BTTF_Graph.getInstance().getFramedGraph();
						BTTF_Initializer.createQuotes(framedGraph);
						grid.loadQuotes();
						initialise.setVisible(false);
					} catch (final Exception e) {
						// Bug means this throws an error
					}
				}

			});
			topLayout.addComponent(initialise);
		}
		return topLayout;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

}
