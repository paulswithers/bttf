package com.bttf.buzzquotebingo.views;

import java.util.List;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.components.HeaderComponent;
import com.bttf.buzzquotebingo.components.Menu;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.Quote;
import com.bttf.buzzquotebingo.util.PageTitleUpdater;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

public class AdminScreen extends VerticalLayout {
	private static final long serialVersionUID = 1L;
	private Menu menu;
	private boolean cardMenuLoaded;
	private static AdminScreen INSTANCE;

	public AdminScreen(MainUI ui, HeaderComponent header) {

		setStyleName("main-screen");

		final HorizontalLayout body = new HorizontalLayout();

		final CssLayout viewContainer = new CssLayout();
		viewContainer.addStyleName("valo-content");
		viewContainer.setSizeFull();

		final Navigator navigator = new Navigator(ui, viewContainer);
		navigator.setErrorView(ErrorView.class);
		setMenu(new Menu(navigator, "BTTF Bingo Admin Portal", null));
		getMenu().addView(new ManageQuotesView(), ManageQuotesView.VIEW_NAME, "Quotes", FontAwesome.COMMENT);
		getMenu().addView(new ManageCardsView(), ManageCardsView.VIEW_NAME, "Cards", FontAwesome.DASHBOARD);
		getMenu().addView(new QuoteView(), QuoteView.VIEW_NAME, "New Quote", FontAwesome.BOOKMARK);
		final List<Quote> quotes = BTTF_Graph.getInstance().getQuotesSortedByProperty("");
		if (!isCardMenuLoaded() && quotes.size() > 9) {
			getMenu().addView(new CardView(), CardView.VIEW_NAME, "My Card", FontAwesome.TABLE);
			setCardMenuLoaded(true);
		} else {
			setCardMenuLoaded(false);
		}
		getMenu().addView(new MarkQuotesSaidView(), MarkQuotesSaidView.VIEW_NAME, "Mark Said", FontAwesome.THUMBS_UP);
		navigator.addView(CardCheckView.VIEW_NAME, new CardCheckView());

		final Button button = new Button("Show Password", new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				final Notification msg = new Notification("", "The admin password is " + MainUI.get().getPassword(), Type.HUMANIZED_MESSAGE, true);
				msg.setDelayMsec(-1);
				msg.show(Page.getCurrent());
			}
		});
		getMenu().createBasicButton(button, FontAwesome.QUESTION);

		navigator.addViewChangeListener(new PageTitleUpdater());

		body.setSizeFull();
		body.addComponent(menu);
		body.addComponent(viewContainer);
		body.setExpandRatio(viewContainer, 1);

		setSizeFull();
		addComponents(header, body);
		setExpandRatio(body, 1);

		INSTANCE = this;
	}

	public static AdminScreen getInstance() {
		return INSTANCE;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public boolean isCardMenuLoaded() {
		return cardMenuLoaded;
	}

	public void setCardMenuLoaded(boolean cardMenuLoaded) {
		this.cardMenuLoaded = cardMenuLoaded;
	}

	private class ViewChangeListener extends PageTitleUpdater {
		@Override
		public void afterViewChange(ViewChangeEvent event) {
			super.afterViewChange(event);
			menu.setActiveView(event.getViewName());
		}
	}

}
