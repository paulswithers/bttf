package com.bttf.buzzquotebingo.views;

import java.util.logging.Logger;

import org.openntf.domino.utils.Strings;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.components.CardForm;
import com.bttf.buzzquotebingo.domino.CardBean;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.Card;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Notification;

@ViewConfig(uri = "CardCheck", displayName = "Card Checker")
public class CardCheckView extends CardView {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(CardCheckView.class.getName());
	public static String VIEW_NAME = "Cardheck";

	public CardCheckView() {
		super();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		try {
			removeAllComponents();
			final String[] params = event.getParameters().split("/");
			if (Strings.isBlankString(params[0])) {
				MainUI.get().getNavigator().navigateTo(ManageCardsView.VIEW_NAME);
				Notification.show("No card passed!", Notification.Type.ERROR_MESSAGE);
			} else {
				final String key = params[0];
				final Card currentCard = BTTF_Graph.getInstance().getFramedGraph().getVertex(key, Card.class);
				if (null == currentCard) {
					Notification.show("Card not found!", Notification.Type.ERROR_MESSAGE);
				} else {
					final CardBean cardBean = new CardBean(currentCard);
					addComponent(new CardForm(cardBean));
				}
			}
		} catch (final Throwable t) {
			Utils.throwError(log, t, "");
		}
	}

}
