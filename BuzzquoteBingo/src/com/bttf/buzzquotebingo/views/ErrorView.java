package com.bttf.buzzquotebingo.views;

import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@ViewConfig(uri = "error", displayName = "Error")
public class ErrorView extends VerticalLayout implements View {

	private Label explanation;

	public ErrorView() {
		setMargin(true);
		setSpacing(true);

		final Label header = new Label("The view could not be found");
		header.addStyleName(Reindeer.LABEL_H1);
		addComponent(header);
		addComponent(explanation = new Label());
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		explanation.setValue(String.format("You tried to navigate to a view ('%s') that does not exist.", event.getViewName()));
	}
}