package com.bttf.buzzquotebingo.views;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.domino.BttfAccessControlService;
import com.bttf.buzzquotebingo.domino.GenericDatabaseUtils;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class AdminLoginScreen extends CssLayout {
	private static final long serialVersionUID = 1L;
	private final LoginListener loginListener;
	private final BttfAccessControlService accessControl;
	private PasswordField password;
	private Button login;
	private Button cancel;
	private final String correctPassword;

	public AdminLoginScreen(BttfAccessControlService accessControl, LoginListener loginListener) {
		this.loginListener = loginListener;
		this.accessControl = accessControl;
		correctPassword = GenericDatabaseUtils.getAdminPassword(accessControl.getPrincipalName());
		buildUI();
		password.focus();
	}

	public void buildUI() {
		addStyleName("login-screen");

		// login form, centered in the available part of the screen
		final Component loginForm = buildLoginForm();

		// layout to center login form when there is sufficient screen space
		// - see the theme for how this is made responsive for various screen
		// sizes
		final VerticalLayout centeringLayout = new VerticalLayout();
		centeringLayout.setStyleName("centering-layout");
		centeringLayout.addComponent(loginForm);
		centeringLayout.setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);

		// information text about logging in
		final CssLayout loginInformation = buildLoginInformation();

		addComponent(centeringLayout);
		addComponent(loginInformation);
	}

	private Component buildLoginForm() {
		final FormLayout loginForm = new FormLayout();

		loginForm.addStyleName("login-form");
		loginForm.setSizeUndefined();
		loginForm.setMargin(false);

		loginForm.addComponent(password = new PasswordField("Password"));
		password.setWidth(8, Unit.EM);
		password.setRequired(true);
		password.setRequiredError("You must enter the password");

		final CssLayout buttons = new CssLayout();
		buttons.setStyleName("buttons");
		loginForm.addComponent(buttons);

		buttons.addComponent(login = new Button("Login"));
		login.setDisableOnClick(true);
		login.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				try {
					login();
				} finally {
					login.setEnabled(true);
				}
			}
		});
		login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		login.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		buttons.addComponent(cancel = new Button("Cancel"));
		cancel.setImmediate(true);
		cancel.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				loginListener.loginAborted();
			}
		});

		return loginForm;
	}

	private CssLayout buildLoginInformation() {
		final CssLayout loginInformation = new CssLayout();
		loginInformation.setStyleName("login-information");
		final Label loginInfoText = new Label(
				"<h1>Login Information</h1>" + "The email address you have logged in with is assigned as the Administrator. Enter the password to gain access to the Admin views. "
						+ "Click 'Cancel' to just load a Bingo Card for yourself.",
				ContentMode.HTML);
		loginInformation.addComponent(loginInfoText);
		return loginInformation;
	}

	private void login() {
		if (StringUtils.equals(correctPassword, password.getValue())) {
			MainUI.get().setPassword(correctPassword);
			loginListener.loginSuccessful();
		} else {
			showNotification(new Notification("Password Wrong!", "Please check the password and try again.", Notification.Type.WARNING_MESSAGE));
			password.setValue("");
			password.focus();
		}
	}

	private void showNotification(Notification notification) {
		// keep the notification visible a little while after moving the
		// mouse, or until clicked
		notification.setDelayMsec(100);
		notification.show(Page.getCurrent());
	}

	public interface LoginListener extends Serializable {
		void loginSuccessful();

		void loginAborted();
	}
}
