### SETUP
Notes - open demo db
Designer - open demo db. Switch theme
Console - open
Browser - open app
Launch ZoomIt

### Opening
SLIDES TO 8

### 1. Twin Pines
documentId, timing of initialisations, ignoreRequestParams, submit button

- Twin_Pines.xsp
Want to create / retrieve document and open it? documentId property is valid
this.documentId="#{....}" - computes multiple times, can't use that for creating documents
*DELETE ALL DOCUMENTS*
Change to $ and show it works...but only because it's attached to a panel
Fails if attached to the XPage - why? Timing, it's computed *before* beforePageLoad, when viewScope is set
*DELETE ALL DOCUMENTS*
Add comment and save
Now change to Lone Pines Mall and save - causes save conflict. Because Submit button used, fine first time, problem second time.

- Twin_Pines_View
Create and save, two documents - because of submit button and single rich text field
Edit and save, just save button - still save conflict, because of single rich text field. Fine if Text field.

### 2. Marty's Family Photo
Keep pages in memory, nostate

- Kill Dave
Open multiple new tabs. Cannot expand/collapse category or navigate.
Export, however, works - because it's window.open(), CSJS. Should set viewState="nostate".
Add viewState="nostate" to dataView. Expand all, collapse all works fine; expand detail, collapse detail works fine; as do switching to pages - both work because setting specific state.
However, collapse Part One, then Part Three - Part One reverts to default state.
Next page, sticks at page 2, because it's next from default state.
Toggle show detail works - show Net tab, because it's CSJS. Explain when / why to use detailsOnClient.

### 3. Marty and Tannens
repeat controls, component tree, dynamic content control

Same Marty in all places, different Tannens.
Show Inspector tab of Debug Toolbar - one for Marty, multiple for Biff, one repeat for Dynamic Content Control
One note, datasource can't compute repeatControls property
Custom Controls inserted into component tree, don't need to pass down

### 4. Power of Love
*BACK TO SLIDES, SLIDE 24-32*
ControllingViewHandler - createView creates component tree
PhaseListener - handles phases being triggered
Add first name but no last name - fails validation
Change refreshId to table1, set disableValidators="true"
Test again
Add text to Order - fails because of data conversion
Do fine and click "Update", shows Save button
Click Save button - nothing happens. Why? Rendered property defaults to false outside phase 6, needs to default true.
Add "return true;" to end of code
*BACK TO SLIDES, SLIDE 34*

### 5. Alternate 1985
Continue in slides

### 6. Indians!
*BACK TO DEMO*
Click on search - explain what resp is null means
Change message and click alert - original message shows
Amend comment

### 7. It's erased!
Click set viewScope, click refresh
Change xsp.properties to keep current pages in memory
Repeat, then navigate to another page
*BACK TO SLIDES, 46*

### SUMMARY
Continue in slides