package com.bttf.buzzquotebingo.components;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.Resource;

public class Menu extends AbstractMenu {

	public Menu(Navigator navigator, String titleLabel, Resource mainImage) {
		super(navigator, titleLabel, mainImage, "http://www.intec.co.uk");
	}

}
