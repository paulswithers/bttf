package com.bttf.buzzquotebingo.components;

import com.bttf.buzzquotebingo.domino.CardBean;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.bttf.buzzquotebingo.graph.Card.HasQuote;
import com.bttf.buzzquotebingo.graph.Quote;
import com.vaadin.server.Page;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;

public class CardForm extends CardFormDesign {
	private static final long serialVersionUID = 1L;
	private CardBean cardBean;

	public CardForm() {

	}

	public CardForm(CardBean cardBean) {
		this.cardBean = cardBean;
		loadContents();
	}

	@Override
	public void loadOneColumnLayout() {
		super.loadOneColumnLayout();
		int idex = 0;
		for (final Quote quote : cardBean.getQuoteObjects()) {
			final HasQuote hasQuote = cardBean.getCard().addQuote(quote);
			getBody().addComponent(createCell(new QuoteBean(quote), hasQuote.isHeardByAttendee(), idex));
			idex++;
		}
	}

	@Override
	public void loadThreeColumnLayout() {
		super.loadThreeColumnLayout();
		int x = 0;
		int idex = 0;
		HorizontalLayout row = new HorizontalLayout();
		row.setWidth(100, Unit.PERCENTAGE);
		for (final Quote quote : cardBean.getQuoteObjects()) {
			final HasQuote hasQuote = cardBean.getCard().addQuote(quote);
			row.addComponent(createCell(new QuoteBean(quote), hasQuote.isHeardByAttendee().booleanValue(), idex));
			x++;
			idex++;
			if (x == 3) {
				getBody().addComponent(row);
				getBody().setExpandRatio(row, 0.3f);
				row = new HorizontalLayout();
				row.setWidth(100, Unit.PERCENTAGE);
				x = 0;
			}
		}
	}

	@Override
	protected void toggleHeard(int idex, boolean heard) {
		cardBean.setHeard(idex, heard);
	}

	@Override
	protected void markCard() {
		final Notification msg = new Notification(cardBean.isWinningClaim(), Notification.Type.HUMANIZED_MESSAGE);
		msg.setDelayMsec(5000);
		msg.show(Page.getCurrent());
	}

	@Override
	protected String getName() {
		return getCardBean().getName();
	}

	public CardBean getCardBean() {
		return cardBean;
	}

	public void setCardBean(CardBean cardBean) {
		this.cardBean = cardBean;
	}

}
