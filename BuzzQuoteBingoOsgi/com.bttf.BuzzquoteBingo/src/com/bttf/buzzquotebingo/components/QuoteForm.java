package com.bttf.buzzquotebingo.components;

import java.util.logging.Logger;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.Film;
import com.bttf.buzzquotebingo.graph.Quote;
import com.bttf.buzzquotebingo.graph.SectionName;
import com.bttf.buzzquotebingo.views.ManageQuotesView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class QuoteForm extends QuoteFormDesign {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(QuoteForm.class.getName());
	private QuoteBean thisQuote = new QuoteBean();
	private BeanFieldGroup<QuoteBean> beanFields = new BeanFieldGroup<QuoteBean>(QuoteBean.class);

	public QuoteForm() {
		super();
		loadFilmItems();
		loadSectionItems();

		// perform validation and enable/disable buttons while editing
		final ValueChangeListener valueListener = new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				formHasChanged();
			}
		};
		summary.addValueChangeListener(valueListener);
		quote.addValueChangeListener(valueListener);
		film.addValueChangeListener(valueListener);
		whoBy.addValueChangeListener(valueListener);
		whoTo.addValueChangeListener(valueListener);
		section.addValueChangeListener(valueListener);

		save.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				if (getThisQuote().saveQuote()) {
					setMode(false);
				}
			}

		});

		edit.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				setMode(true);
			}
		});

		delete.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				final Quote currentQuote = BTTF_Graph.getInstance().getFramedGraph().getVertex(summary.getValue(),
						Quote.class);
				// TODO: Remove all
				MainUI.get().getNavigator().navigateTo(ManageQuotesView.VIEW_NAME);
			}
		});

		cancel.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				MainUI.get().getNavigator().navigateTo(ManageQuotesView.VIEW_NAME);
			}
		});
	}

	public void setMode(boolean editMode) {
		if (editMode) {
			edit.setVisible(false);
			save.setVisible(true);
			delete.setVisible(false);
		} else {
			edit.setVisible(true);
			save.setVisible(false);
			delete.setVisible(true);
		}
		getBeanFields().setReadOnly(!editMode);
		if (editMode) {
			setInitialFocus();
		}
	}

	public void loadFilmItems() {
		for (int i = 0; i < Film.values().length; i++) {
			film.addItem(Film.values()[i].getValue());
		}
	}

	public void loadSectionItems() {
		for (int i = 0; i < SectionName.values().length; i++) {
			section.addItem(SectionName.values()[i].getValue());
		}
	}

	public void setInitialFocus() {
		summary.focus();
	}

	private void formHasChanged() {
		// only documents that have been saved should be removable
		delete.setEnabled(false);
	}

	public Button getSaveButton() {
		return save;
	}

	public Button getEditButton() {
		return edit;
	}

	public Button getDeleteButton() {
		return delete;
	}

	public QuoteBean getThisQuote() {
		return thisQuote;
	}

	public void setThisQuote(QuoteBean thisQuote) {
		this.thisQuote = thisQuote;
	}

	public BeanFieldGroup<QuoteBean> getBeanFields() {
		return beanFields;
	}

	public void setBeanFields(BeanFieldGroup<QuoteBean> beanFields) {
		this.beanFields = beanFields;
	}

}
