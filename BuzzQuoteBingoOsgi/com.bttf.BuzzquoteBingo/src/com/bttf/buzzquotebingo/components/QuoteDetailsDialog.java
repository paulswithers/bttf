package com.bttf.buzzquotebingo.components;

import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class QuoteDetailsDialog extends Window {
	private static final long serialVersionUID = 1L;
	private QuoteBean currentQuote;

	public QuoteDetailsDialog() {
		super("Quote");
		center();
	}

	public QuoteBean getCurrentQuote() {
		return currentQuote;
	}

	public void setCurrentQuote(QuoteBean currentQuote) {
		this.currentQuote = currentQuote;
	}

	public void loadContent() {
		final VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		setStyleName("card-label-window");
		final Label label = new Label(currentQuote.getQuote());
		label.setStyleName("card-label");
		final Label details = new Label();
		if (currentQuote.getWhoBy().equals(currentQuote.getWhoTo())) {
			details.setCaption("Said by " + currentQuote.getWhoBy() + " to self");
		} else {
			details.setCaption("Said by " + currentQuote.getWhoBy() + " to " + currentQuote.getWhoTo());
		}
		details.setStyleName("card-label-small-bold");
		final Label film = new Label(currentQuote.getFilm());
		film.setStyleName("card-label-film");
		final HorizontalLayout whoFilm = new HorizontalLayout();
		whoFilm.setSizeFull();
		whoFilm.addComponents(details, film);
		content.addComponents(label, whoFilm);
		content.setExpandRatio(label, 5);
		content.setExpandRatio(whoFilm, 2);
		content.setMargin(true);
		setContent(content);
	}

}
