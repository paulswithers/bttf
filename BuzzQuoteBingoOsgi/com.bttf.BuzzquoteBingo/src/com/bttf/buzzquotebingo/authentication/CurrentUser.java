package com.bttf.buzzquotebingo.authentication;

import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openntf.osgiworlds.DefaultDominoApplicationConfig;

import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.domino.BttfAccessControlService;
import com.vaadin.server.VaadinServletRequest;

public class CurrentUser {

	private static Logger log = Logger.getLogger(CurrentUser.class.getName());

	/**
	 * The attribute key used to store the username in the session.
	 */
	public static final String CURRENT_USER_SESSION_ATTRIBUTE_KEY = CurrentUser.class.getCanonicalName();

	private static String getUserFullName(String userEmail) {
		if (StringUtils.contains(userEmail, "/")) {
			return userEmail;
		} else {
			if ("true".equals(Utils.getCurrentRequest().getWrappedSession().getAttribute(BttfAccessControlService.USER_ADMIN_KEY))) {
				return "CN=" + userEmail + "/OU=BTTF_IBMConnect2015Admin/O=Intec";
			}
			return "CN=" + userEmail + "/OU=BTTF_IBMConnect2015/O=Intec";
		}
	}

	/**
	 * Returns the name of the current user stored in the current session, or an
	 * empty string if no user name is stored.
	 *
	 * @throws IllegalStateException
	 *             if the current session cannot be accessed.
	 */
	public static String get() {
		final String currentUser = (String) Utils.getCurrentRequest().getWrappedSession().getAttribute(CURRENT_USER_SESSION_ATTRIBUTE_KEY);
		if (currentUser == null) {
			return "";
		} else {
			return currentUser;
		}
	}

	/**
	 * Sets the name of the current user and stores it in the current session.
	 * Using a {@code null} username will remove the username from the session.
	 *
	 * @throws IllegalStateException
	 *             if the current session cannot be accessed.
	 */
	public static void set(String currentUserEmail) {

		if (currentUserEmail == null) {
			Utils.getCurrentRequest().getWrappedSession().removeAttribute(CURRENT_USER_SESSION_ATTRIBUTE_KEY);
			Utils.getCurrentRequest().getWrappedSession().removeAttribute(DefaultDominoApplicationConfig.CONTEXTPARAM_SESSION_IDENTITY);
			// Set domino application identity
			final VaadinServletRequest s = (VaadinServletRequest) Utils.getCurrentRequest();
			DefaultDominoApplicationConfig.setDominoFullName("Anonymous");
			log.info("Identity set to anoymous");
		} else {
			Utils.getCurrentRequest().getWrappedSession().setAttribute(CURRENT_USER_SESSION_ATTRIBUTE_KEY, currentUserEmail);
			Utils.getCurrentRequest().getWrappedSession().setAttribute(DefaultDominoApplicationConfig.CONTEXTPARAM_SESSION_IDENTITY, getUserFullName(currentUserEmail));
			// Set domino application identity
			final VaadinServletRequest s = (VaadinServletRequest) Utils.getCurrentRequest();
			DefaultDominoApplicationConfig.setDominoFullName(getUserFullName(currentUserEmail));
			log.info("Identity set to: " + DefaultDominoApplicationConfig.getDominoFullName());
		}
	}
}
