package com.bttf.buzzquotebingo.views;

import java.io.Serializable;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.authentication.CurrentUser;
import com.bttf.buzzquotebingo.domino.BttfAccessControlService;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@ViewConfig(uri = "login", displayName = "Login")
public class LoginScreen extends CssLayout {
	private static final long serialVersionUID = 1L;
	public static final String NAME = "login";
	private TextField name;
	private TextField email;
	private Button login;
	private final LoginListener loginListener;
	private final BttfAccessControlService accessControl;

	public LoginScreen(BttfAccessControlService accessControl, LoginListener loginListener) {
		this.loginListener = loginListener;
		this.accessControl = accessControl;
		buildUI();
		name.focus();
	}

	private void buildUI() {
		addStyleName("login-screen");

		// login form, centered in the available part of the screen
		final Component loginForm = buildLoginForm();

		// layout to center login form when there is sufficient screen space
		// - see the theme for how this is made responsive for various screen
		// sizes
		final VerticalLayout centeringLayout = new VerticalLayout();
		centeringLayout.setStyleName("centering-layout");
		centeringLayout.addComponent(loginForm);
		centeringLayout.setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);

		// information text about logging in
		final CssLayout loginInformation = buildLoginInformation();

		addComponent(loginInformation);
		addComponent(centeringLayout);
	}

	private Component buildLoginForm() {
		final FormLayout loginForm = new FormLayout();

		loginForm.addStyleName("login-form");
		loginForm.setSizeUndefined();
		loginForm.setMargin(false);

		loginForm.addComponent(name = new TextField("Name"));
		name.setWidth(15, Unit.EM);
		name.setRequired(true);
		name.setRequiredError("Please enter your name");
		name.focus();
		loginForm.addComponent(email = new TextField("Email"));
		email.setWidth(15, Unit.EM);
		email.setRequired(true);
		email.setRequiredError("Please enter a valid email");
		email.addValidator(new EmailValidator("Please enter a valid email"));
		final CssLayout buttons = new CssLayout();
		buttons.setStyleName("buttons");
		loginForm.addComponent(buttons);

		buttons.addComponent(login = new Button("Login"));
		login.setDisableOnClick(true);
		login.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				try {
					if (email.isValid() && name.isValid()) {
						login();
					}
				} finally {
					login.setEnabled(true);
				}
			}
		});
		login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		login.addStyleName(ValoTheme.BUTTON_FRIENDLY);

		return loginForm;
	}

	private CssLayout buildLoginInformation() {
		final CssLayout loginInformation = new CssLayout();
		loginInformation.setStyleName("login-information");
		final Label loginInfoText = new Label("<h1>Login Information</h1>" + "Enter your name and email address. This will allow the 'Bingo Caller' to know who you are "
				+ "and allow you to log back in at a later date and retrieve your card.", ContentMode.HTML);
		loginInformation.addComponent(loginInfoText);
		return loginInformation;
	}

	private void login() {
		if (accessControl.signIn(email.getValue())) {
			MainUI.get().setCommonName(name.getValue());
			MainUI.get().setEmail(CurrentUser.get());
			loginListener.loginSuccessful(); // Database not initialised yet!
		} else {
			// Really, this isn't possible unless there's an error
		}
	}

	public interface LoginListener extends Serializable {
		void loginSuccessful();
	}

}
