package com.bttf.buzzquotebingo.views;

import java.util.logging.Logger;

import com.bttf.buzzquotebingo.components.CardsGrid;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@ViewConfig(uri = "Cards", displayName = "Cards")
public class ManageCardsView extends CssLayout implements View {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ManageCardsView.class.getName());
	public static String VIEW_NAME = "Cards";
	private boolean loaded = false;
	private final CardsGrid grid;
	private HorizontalLayout body;

	public ManageCardsView() {
		setSizeFull();
		grid = new CardsGrid();
		grid.addSelectionListener(new SelectionListener() {

			@Override
			public void select(SelectionEvent event) {

			}

		});
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (!isLoaded()) {
			if (setContent()) {
				setLoaded(true);
			}
		}
	}

	public boolean setContent() {
		boolean retVal_ = false;
		try {
			final HorizontalLayout topLayout = createTopBar();
			final VerticalLayout barAndGridLayout = new VerticalLayout();
			barAndGridLayout.addComponent(topLayout);
			barAndGridLayout.addComponent(grid);
			barAndGridLayout.setMargin(true);
			barAndGridLayout.setSpacing(true);
			barAndGridLayout.setSizeFull();
			barAndGridLayout.setExpandRatio(grid, 1);
			barAndGridLayout.setStyleName("crud-main-layout");

			addComponent(barAndGridLayout);

			grid.loadCards();
			retVal_ = true;
		} catch (final Exception e) {

		}
		return retVal_;
	}

	public HorizontalLayout createTopBar() {
		final HorizontalLayout topLayout = new HorizontalLayout();

		final Button button = new Button("Refresh");
		button.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		button.setIcon(FontAwesome.REFRESH);
		button.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				grid.loadCards();
			}

		});

		topLayout.setSpacing(true);
		topLayout.setWidth("100%");
		topLayout.addComponents(button);
		topLayout.setStyleName("top-bar");
		return topLayout;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public HorizontalLayout getBody() {
		return body;
	}

	public void setBody(HorizontalLayout body) {
		this.body = body;
	}

}
