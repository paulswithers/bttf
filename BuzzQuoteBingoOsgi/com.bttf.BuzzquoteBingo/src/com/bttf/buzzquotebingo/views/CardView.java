package com.bttf.buzzquotebingo.views;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.components.CardForm;
import com.bttf.buzzquotebingo.domino.CardBean;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@ViewConfig(uri = "Card", displayName = "My Card")
public class CardView extends CssLayout implements View {
	private static final long serialVersionUID = 1L;
	public static String VIEW_NAME = "Card";
	private boolean loaded = false;
	private CardForm cardForm;

	public CardView() {
		setSizeFull();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		final CardBean cardBean = new CardBean(MainUI.get().getEmail());
		setCardForm(new CardForm(cardBean));
		if (!isLoaded()) {
			if (setContent()) {
				setLoaded(true);
			}
		}
	}

	public boolean setContent() {
		boolean retVal_ = false;
		try {
			addComponent(getCardForm());
			retVal_ = true;
		} catch (final Exception e) {
			Utils.throwError(null, e, "");
		}
		return retVal_;
	}

	/**
	 * Shows an error message on the screen
	 *
	 * @param msg
	 *            String error to display
	 */
	public void showError(String msg) {
		Notification.show(msg, Type.ERROR_MESSAGE);
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public CardForm getCardForm() {
		return cardForm;
	}

	public void setCardForm(CardForm cardForm) {
		this.cardForm = cardForm;
	}

}
