package com.bttf.buzzquotebingo.views;

import java.util.List;
import java.util.logging.Logger;

import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.domino.QuoteBean;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.SectionName;
import com.bttf.buzzquotebingo.util.ViewConfig;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@ViewConfig(uri = "QuotesSaid", displayName = "Quotes Flagging")
public class MarkQuotesSaidView extends CssLayout implements View {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(MarkQuotesSaidView.class.getName());
	public static String VIEW_NAME = "QuotesSaid";
	private VerticalLayout body;
	private boolean loaded = false;

	public MarkQuotesSaidView() {
		setSizeFull();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (!isLoaded()) {
			if (setContent()) {
				setLoaded(true);
			}
		}

	}

	public boolean setContent() {
		boolean retVal_ = false;
		try {
			final Panel panel = new Panel();
			if (null == getBody()) {
				setBody(new VerticalLayout());
			}

			loadBody();

			panel.setContent(getBody());
			panel.setWidth(100, Unit.PERCENTAGE);
			panel.setHeight(100, Unit.PERCENTAGE);
			addComponent(panel);

			retVal_ = true;
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

	private void loadBody() {
		getBody().removeAllComponents();
		getBody().setStyleName("card-cell");
		getBody().setSizeUndefined();
		getBody().setWidth(100, Unit.PERCENTAGE);

		int quoteCount = 0;
		for (int i = 0; i < SectionName.values().length; i++) {
			final SectionName section = SectionName.values()[i];
			// if (!section.equals(SectionName.NONE)) {
			final List<QuoteBean> quotes = BTTF_Graph.getInstance().loadQuoteBeansForSection(section);
			if (quotes.size() > 0) {
				quoteCount = quoteCount + quotes.size();
				final Label label = new Label(section.getValue());
				label.addStyleName(ValoTheme.LABEL_H3);
				label.addStyleName("quote-label");
				getBody().addComponent(label);
				for (final QuoteBean quoteBean : quotes) {
					final HorizontalLayout row = new HorizontalLayout();
					row.setWidth(80, Unit.PERCENTAGE);
					row.setHeight(100, Unit.PERCENTAGE);
					row.setStyleName("card-cell");
					final Label quote = new Label(quoteBean.getQuote());
					final Button button = new Button();
					if (quoteBean.isSaidByPresenter()) {
						button.setIcon(FontAwesome.THUMBS_O_UP);
						button.setStyleName(ValoTheme.BUTTON_FRIENDLY);
					} else {
						button.setIcon(FontAwesome.THUMBS_DOWN);
						button.setStyleName(ValoTheme.BUTTON_DANGER);
					}
					button.addClickListener(new ClickListener() {

						@Override
						public void buttonClick(ClickEvent event) {
							quoteBean.markSaid();
							if (quoteBean.isSaidByPresenter()) {
								button.setIcon(FontAwesome.THUMBS_O_UP);
								button.setStyleName(ValoTheme.BUTTON_FRIENDLY);
							} else {
								button.setIcon(FontAwesome.THUMBS_DOWN);
								button.setStyleName(ValoTheme.BUTTON_DANGER);
							}
						}

					});
					row.addComponents(quote, button);
					row.setExpandRatio(quote, 1.0f);
					getBody().addComponent(row);
				}
			}
			// }
		}

		if (quoteCount > 9) {
			if (!AdminScreen.getInstance().isCardMenuLoaded()) {
				AdminScreen.getInstance().getMenu().addView(new CardView(), CardView.VIEW_NAME, "My Card", FontAwesome.TABLE);
				AdminScreen.getInstance().setCardMenuLoaded(true);
			}
		}
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public VerticalLayout getBody() {
		return body;
	}

	public void setBody(VerticalLayout body) {
		this.body = body;
	}

}
