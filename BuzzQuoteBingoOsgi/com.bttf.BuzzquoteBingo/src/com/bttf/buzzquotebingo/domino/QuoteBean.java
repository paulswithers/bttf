package com.bttf.buzzquotebingo.domino;

import java.io.Serializable;
import java.util.Iterator;
import java.util.logging.Logger;

import org.openntf.domino.utils.Strings;

import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.components.QuoteForm;
import com.bttf.buzzquotebingo.components.QuotesGrid;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.FilmCharacter;
import com.bttf.buzzquotebingo.graph.Quote;
import com.bttf.buzzquotebingo.graph.Section;
import com.bttf.buzzquotebingo.graph.SectionName;

public class QuoteBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(QuoteForm.class.getName());
	private String summary;
	private String quote;
	private String film;
	private String whoBy;
	private String whoTo;
	private String section;
	private boolean saidByPresenter;

	public QuoteBean() {

	}

	public QuoteBean(Quote quote) {
		setSummary(quote.getSummary());
		setQuote(quote.getQuote());
		setFilm(quote.getFilm());
		final Iterable<FilmCharacter> byIt = quote.getQuotedBy();
		if (byIt.iterator().hasNext()) {
			setWhoBy(byIt.iterator().next().getName());
		}
		final Iterable<FilmCharacter> toIt = quote.getSaidTo();
		if (toIt.iterator().hasNext()) {
			setWhoTo(toIt.iterator().next().getName());
		}
		final Iterable<Section> sec = quote.getSection();
		if (sec.iterator().hasNext()) {
			setSection(sec.iterator().next().getName());
		}
		setSaidByPresenter(quote.isSaidByPresenter());
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getFilm() {
		return film;
	}

	public void setFilm(String film) {
		this.film = film;
	}

	public String getWhoBy() {
		return whoBy;
	}

	public void setWhoBy(String whoBy) {
		this.whoBy = whoBy;
	}

	public String getWhoTo() {
		return whoTo;
	}

	public void setWhoTo(String whoTo) {
		this.whoTo = whoTo;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public boolean isSaidByPresenter() {
		return saidByPresenter;
	}

	public void setSaidByPresenter(boolean saidByPresenter) {
		this.saidByPresenter = saidByPresenter;
	}

	public void loadQuote(String key) {
		try {
			final Quote quote = BTTF_Graph.getInstance().getFramedGraph().getVertex(key, Quote.class);
			if (!Strings.isBlankString(quote.getFilm())) {
				setSummary(quote.getSummary());
				setQuote(quote.getQuote());
				setFilm(quote.getFilm());
				final Iterable<FilmCharacter> byIt = quote.getQuotedBy();
				if (byIt.iterator().hasNext()) {
					setWhoBy(byIt.iterator().next().getName());
				}
				final Iterable<FilmCharacter> toIt = quote.getSaidTo();
				if (toIt.iterator().hasNext()) {
					setWhoTo(toIt.iterator().next().getName());
				}
				final Iterable<Section> sec = quote.getSection();
				if (sec.iterator().hasNext()) {
					setSection(sec.iterator().next().getName());
				}
				setSaidByPresenter(quote.isSaidByPresenter());
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
	}

	public boolean saveQuote() {
		final QuoteBean oldQuote = null;
		try {
			final Quote currentQuote = BTTF_Graph.getInstance().getFramedGraph().addVertex(getSummary(), Quote.class);
			currentQuote.setSummary(getSummary());
			currentQuote.setQuote(getQuote());
			currentQuote.setFilm(getFilm());
			Iterator<FilmCharacter> it = currentQuote.getQuotedBy().iterator();
			while (it.hasNext()) {
				currentQuote.removeQuotedBy(it.next());
			}
			final FilmCharacter by = BTTF_Graph.getInstance().getFramedGraph().addVertex(getWhoBy(), FilmCharacter.class);
			if (Strings.isBlankString(by.getName())) {
				by.setName(getWhoBy());
			}
			currentQuote.addQuotedBy(by);
			it = currentQuote.getSaidTo().iterator();
			while (it.hasNext()) {
				currentQuote.removeSaidTo(it.next());
			}
			final FilmCharacter to = BTTF_Graph.getInstance().getFramedGraph().addVertex(getWhoTo(), FilmCharacter.class);
			if (Strings.isBlankString(to.getName())) {
				to.setName(getWhoTo());
			}
			currentQuote.addSaidTo(to);
			final Iterator<Section> secIt = currentQuote.getSection().iterator();
			while (secIt.hasNext()) {
				currentQuote.removeUsedIn(secIt.next());
			}
			for (int i = 0; i < SectionName.values().length; i++) {
				if (SectionName.values()[i].getValue().equals(getSection())) {
					final Section sec = BTTF_Graph.getInstance().getFramedGraph().addVertex(SectionName.values()[i].name(), Section.class);
					if (Strings.isBlankString(sec.getName())) {
						sec.setName(getSection());
					}
					currentQuote.addUsedIn(sec);
				}
			}
			currentQuote.setSaidByPresenter(isSaidByPresenter());
			BTTF_Graph.getInstance().getFramedGraph().commit();
			QuotesGrid.getInstance().addBean(this);
			return true;
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
			if (null != oldQuote) {
				QuotesGrid.getInstance().addBean(oldQuote);
			}
			return false;
		}
	}

	public void markSaid() {
		final Quote currentQuote = BTTF_Graph.getInstance().getFramedGraph().addVertex(getSummary(), Quote.class);
		currentQuote.setSaidByPresenter(!isSaidByPresenter());
		BTTF_Graph.getInstance().getFramedGraph().commit();
		setSaidByPresenter(!isSaidByPresenter());
	}

	@Override
	public boolean equals(Object obj) {
		return getSummary().equals(((QuoteBean) obj).getSummary());
	}

}
