package com.bttf.buzzquotebingo.domino;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openntf.domino.big.NoteCoordinate;
import org.openntf.domino.utils.Strings;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.graph.BTTF_Graph;
import com.bttf.buzzquotebingo.graph.Card;
import com.bttf.buzzquotebingo.graph.Card.HasQuote;
import com.bttf.buzzquotebingo.graph.Quote;
import com.bttf.buzzquotebingo.graph.Section;
import com.bttf.buzzquotebingo.graph.SectionName;

public class CardBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(CardBean.class.getName());
	private String key;
	private String name;
	private Card card;
	private List<Quote> quoteObjects;
	private List<String> quotes;
	private int usedQuotes;
	private int heardQuotes;
	private boolean winner;

	public CardBean() {

	}

	public CardBean(String email) {
		loadCard(email);
	}

	public CardBean(Card card) {
		setCard(card);
		setBeanLists();
		setName(card.getName());
		setWinner(getCard().isWinner());
		final NoteCoordinate key = (NoteCoordinate) getCard().asVertex().getId();
		setKey(key.toString());
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Quote> getQuoteObjects() {
		return quoteObjects;
	}

	public void setQuoteObjects(List<Quote> quoteObjects) {
		this.quoteObjects = quoteObjects;
	}

	public List<String> getQuotes() {
		return quotes;
	}

	public void setQuotes(List<String> quotes) {
		this.quotes = quotes;
	}

	public int getUsedQuotes() {
		return usedQuotes;
	}

	public void setUsedQuotes(int usedQuotes) {
		this.usedQuotes = usedQuotes;
	}

	public int getHeardQuotes() {
		return heardQuotes;
	}

	public void setHeardQuotes(int heardQuotes) {
		this.heardQuotes = heardQuotes;
	}

	public boolean isWinner() {
		return winner;
	}

	public void setWinner(boolean winner) {
		this.winner = winner;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public void loadCard(String email) {
		try {
			final Card currentCard = BTTF_Graph.getInstance().getFramedGraph().addVertex(email.toLowerCase(), Card.class);
			setCard(currentCard);
			if (!Strings.isBlankString(getCard().getName())) {
				setBeanLists();
			} else {
				initialiseCard(email);
			}
			setName(getCard().getName());
			setWinner(getCard().isWinner());
			final NoteCoordinate key = (NoteCoordinate) getCard().asVertex().getId();
			setKey(key.toString());
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
	}

	private void setBeanLists() {
		final Iterable<Quote> allQuotes = getCard().getQuotes();
		final Iterator<Quote> it = allQuotes.iterator();
		final List<Quote> quoteObjList = new ArrayList<Quote>();
		final List<String> quoteList = new ArrayList<String>();
		int validQuotes = 0;
		int heardQuotes = 0;
		while (it.hasNext()) {
			final Quote q = it.next();
			quoteObjList.add(q);
			quoteList.add(q.getQuote());
			final Iterable<Section> sec = q.getSection();
			if (sec.iterator().hasNext()) {
				if (!SectionName.NONE.getValue().equals(sec.iterator().next().getName())) {
					validQuotes++;
				}
			}
			final HasQuote hasQuote = getCard().addQuote(q);
			if (hasQuote.isHeardByAttendee()) {
				heardQuotes++;
			}
		}
		setQuoteObjects(quoteObjList);
		setQuotes(quoteList);
		setUsedQuotes(validQuotes);
		setHeardQuotes(heardQuotes);
	}

	public void initialiseCard(String email) {
		final List<Quote> quoteObjList = new ArrayList<Quote>();
		final List<String> quoteList = new ArrayList<String>();
		int validQuotes = 0;
		getCard().setName(MainUI.get().getCommonName());
		final List<Quote> allQuotes = BTTF_Graph.getInstance().getQuotesSortedByProperty("");
		for (int x = 0; x < 9; x++) {
			boolean foundValue = false;
			while (!foundValue) {
				final Random randomizer = new Random();
				final Quote randomQuote = allQuotes.get(randomizer.nextInt(allQuotes.size()));
				if (!quoteObjList.contains(randomQuote)) {
					quoteObjList.add(randomQuote);
					quoteList.add(randomQuote.getQuote());
					final HasQuote edge = getCard().addQuote(randomQuote);
					edge.setHeardByAttendee(false);
					foundValue = true;
					final Iterable<Section> sec = randomQuote.getSection();
					if (sec.iterator().hasNext()) {
						if (!SectionName.NONE.getValue().equals(sec.iterator().next().getName())) {
							validQuotes++;
						}
					}
				}
			}
		}
		BTTF_Graph.getInstance().getFramedGraph().commit();
		setQuoteObjects(quoteObjList);
		setQuotes(quoteList);
		setHeardQuotes(0);
		setUsedQuotes(validQuotes);
	}

	public void setHeard(int idex, boolean heard) {
		final HasQuote hasQuote = getCard().addQuote(getQuoteObjects().get(idex));
		hasQuote.setHeardByAttendee(heard);
		BTTF_Graph.getInstance().getFramedGraph().commit();
		if (heard) {
			setHeardQuotes(getHeardQuotes() + 1);
		} else {
			setHeardQuotes(getHeardQuotes() - 1);
		}
	}

	public String isWinningClaim() {
		String retVal_ = "";
		try {
			final ArrayList<String> unsaidQuotes = new ArrayList<String>();
			for (final Quote quote : getQuoteObjects()) {
				if (!quote.isSaidByPresenter()) {
					unsaidQuotes.add(quote.getQuote());
				}
			}
			if (unsaidQuotes.isEmpty()) {
				setWinner(true);
				getCard().setWinner(true);
				BTTF_Graph.getInstance().getFramedGraph().commit();
				retVal_ = "Congratulations! You are the winner!";
			} else {
				setWinner(false);
				retVal_ = "Sorry, you're missing the following quotes:\n" + StringUtils.join(unsaidQuotes.toArray(), "\n");
			}
		} catch (final Exception e) {
			Utils.throwError(log, e, "");
		}
		return retVal_;
	}

}
