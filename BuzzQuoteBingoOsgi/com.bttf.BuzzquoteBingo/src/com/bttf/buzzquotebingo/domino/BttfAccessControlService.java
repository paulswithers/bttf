package com.bttf.buzzquotebingo.domino;

import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;

import com.bttf.buzzquotebingo.MainUI;
import com.bttf.buzzquotebingo.Utils;
import com.bttf.buzzquotebingo.authentication.BasicAccessControlService;
import com.bttf.buzzquotebingo.authentication.CurrentUser;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class BttfAccessControlService extends BasicAccessControlService {
	private final static Logger log = Logger.getLogger(BttfAccessControlService.class.getName());
	public static final String USER_ADMIN_KEY = "com.bttf.admin";

	@Override
	public boolean signIn(String userEmail) {

		userEmail = StringUtils.replace(userEmail, "@", "_");

		log.info("signIn " + userEmail);

		if (StringUtils.isEmpty(userEmail)) {
			return false;
		}

		checkUserIsAdmin(userEmail);

		CurrentUser.set(userEmail);

		log.info("clearing session identity");
		Factory.getSession(SessionType.CURRENT).clearIdentity();

		return true;
	}

	public void checkUserIsAdmin(String userEmail) {
		try {
			if (!GenericDatabaseUtils.dbExists()) {
				setUserAdmin();
				if (GenericDatabaseUtils.initialiseDbs(userEmail)) {
					Notification msg = new Notification("Databases Initialised",
							"Congratulations, the databases are initialised. You are the admin and your password is "
									+ MainUI.get().getPassword()
									+ ".<br/> You will be prompted for the password before being granted access to the admin console.",
							Type.HUMANIZED_MESSAGE, true);
					msg.setDelayMsec(-1);
					msg.show(Page.getCurrent());
				} else {
					Notification.show("Cannot create databases", "Trying to create in base folder "
							+ Utils.getBaseFolder() + " using signer ID (" + Utils.getSignerName() + ")",
							Type.ERROR_MESSAGE);
				}
			} else if (GenericDatabaseUtils.adminProfileExists(userEmail)) {
				setUserAdmin();
			}
		} catch (Exception e) {
			Utils.throwError(log, e, "");
		}
	}

	public void setUserAdmin() {
		Utils.getCurrentRequest().getWrappedSession().setAttribute(USER_ADMIN_KEY, "true");
	}

	private Boolean isUserAdmin() {
		String adminKeyString = (String) Utils.getCurrentRequest().getWrappedSession().getAttribute(USER_ADMIN_KEY);
		return Boolean.valueOf(adminKeyString);
	}

	@Override
	public boolean isUserInRole(String role) {
		if ("admin".equals(role)) {
			return isUserAdmin();
		}
		return super.isUserInRole(role);
	}

}
