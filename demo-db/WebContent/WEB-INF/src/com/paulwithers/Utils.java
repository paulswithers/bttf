package com.paulwithers;

import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import lotus.domino.Database;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.ibm.xsp.application.BasicStateManagerImpl.ViewHolder;
import com.ibm.xsp.designer.context.XSPContext;
import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.paulwithers.openLog.OpenLogUtil;

public class Utils {
	public static String getSessionStateInfo() {
		String retVal = "";
		try {
			Map sessScope = ExtLibUtil.getSessionScope();
			ViewHolder views = (ViewHolder) sessScope.get("__XSP_STATE_BASIC");
			XSPContext context = (XSPContext) ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "context");
			String max = context.getProperty("xsp.persistence.tree.maxviews");
			int count = views.getViewRoots().length;
			retVal = "Max number of views is " + max + ", current count is " + Integer.toString(count);
		} catch (Throwable t) {
			OpenLogUtil.logError(t);
		}
		return retVal;
	}

	public static void killDave() {
		try {
			System.out.println("Outputting Excel...");
			Database db = ExtLibUtil.getCurrentDatabase();
			View people = db.getView("AllPeople");
			ViewNavigator nav = people.createViewNav();

			HSSFWorkbook wb = new HSSFWorkbook();

			//Add header style (Bold)
			HSSFCellStyle headerStyle = wb.createCellStyle();
			HSSFFont headerFont = wb.createFont();
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			headerStyle.setFont(headerFont);
			headerStyle.setWrapText(true);
			headerStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);

			// Add basic wrap style
			HSSFCellStyle wrapStyle = wb.createCellStyle();
			wrapStyle.setWrapText(true);
			wrapStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);

			HSSFSheet sheet = wb.createSheet();
			// Add headers
			HSSFRow row = sheet.createRow(0);
			HSSFCell hCell = row.createCell(0);
			hCell.setCellValue("Position");
			hCell.setCellStyle(headerStyle);
			hCell = row.createCell(1);
			hCell.setCellValue("Name");
			hCell.setCellStyle(headerStyle);
			hCell = row.createCell(2);
			hCell.setCellValue("Films");
			hCell.setCellStyle(headerStyle);

			int i = 1;
			ViewEntry ent = nav.getFirst();
			while (null != ent) {
				Vector vec = ent.getColumnValues();
				row = sheet.createRow(i);
				hCell = row.createCell(0);
				hCell.setCellValue(vec.get(0).toString());
				hCell = row.createCell(1);
				hCell.setCellValue(vec.get(3).toString());
				hCell = row.createCell(2);
				hCell.setCellValue(vec.get(4).toString());
				ent = nav.getNext();
				i++;
			}

			// Write out to servletResponse
			HttpServletResponse pageResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
					.getExternalContext().getResponse();
			ServletOutputStream pageOutput = pageResponse.getOutputStream();
			pageResponse.setContentType("application/x-ms-excel");
			pageResponse.setHeader("Cache-Control", "no-cache");
			pageResponse.setHeader("Content-Disposition", "inline; filename=export.xls");
			wb.write(pageOutput);
			pageOutput.flush();
			pageOutput.close();

			//  Terminate the request processing lifecycle.
			System.out.println("Done Excel...");
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Throwable t) {
			OpenLogUtil.logError(t);
		}
	}

	public static void debugRequestScope(String from) {
		try {
			Map r = ExtLibUtil.getRequestScope();
			if (null != r) {
				String value = "";
				System.out.println("Outputting requestScope in " + from + ": " + r.size() + " keys");
				for (Object key : r.keySet()) {
					value = value + key + ",";
				}
				System.out.println(value);
				OpenLogUtil.logEvent(new Throwable(), "Outputting requestScope in " + from + ": " + r.size()
						+ " keys\n" + value, Level.FINE, null);
			}
		} catch (Throwable t) {
			System.out.println("Error" + from);
		}
	}

}
