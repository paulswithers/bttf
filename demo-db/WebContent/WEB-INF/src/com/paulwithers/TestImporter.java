package com.paulwithers;

import java.io.InputStream;

import lotus.domino.View;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class TestImporter {

	public TestImporter() {

	}

	public static boolean loadPeople() {

		boolean retVal = false;
		if (noPeople()) {
			InputStream people = TestImporter.class.getResourceAsStream("People");
			InputStream peopleMapper = TestImporter.class.getResourceAsStream("PeopleMapper");

			retVal = CSV_Util.loadDocsFromFile("Person", people, peopleMapper);
		}
		return retVal;
	}

	public static boolean noPeople() {
		boolean retVal = false;
		try {
			View people = ExtLibUtil.getCurrentDatabase().getView("AllPeople");
			if (null == people.getFirstDocument()) {
				retVal = true;
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return retVal;
	}

	public static void loadRepeats() {
		try {
			View repeat = ExtLibUtil.getCurrentDatabase().getView("RepeatPeople");
			repeat.setAutoUpdate(false);
			if (null == repeat.getFirstDocument()) {
				InputStream people = TestImporter.class.getResourceAsStream("RepeatPeople");
				InputStream peopleMapper = TestImporter.class.getResourceAsStream("RepeatPeopleMapper");

				CSV_Util.loadDocsFromFile("RepeatPerson", people, peopleMapper);
				repeat.refresh();
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
}
