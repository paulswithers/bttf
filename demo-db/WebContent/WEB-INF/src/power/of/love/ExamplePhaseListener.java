package power.of.love;

import javax.faces.component.UIViewRoot;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import com.ibm.xsp.component.UIViewRootEx;
import com.ibm.xsp.context.FacesContextEx;

public class ExamplePhaseListener implements PhaseListener {
	// The PhaseListener interface extends Serializable, so it's recommended
	// that the class specify a version ID:
	private static final long serialVersionUID = 1L;
	private static final int RESTORE_VIEW = 1;
	private static final int APPLY_REQUEST_VALUES = 2;
	private static final int PROCESS_VALIDATIONS = 3;
	private static final int UPDATE_MODEL_VALUES = 4;
	private static final int INVOKE_APPLICATION = 5;
	private static final int RENDER_RESPONSE = 6;
	protected PhaseId phaseId;
	private static ExamplePhaseListener instance;

	public ExamplePhaseListener() {
		instance = this;
	}

	// ------------------------------------------------------------------------

	public static ExamplePhaseListener getInstance() {
		if (null == instance) {
			instance = new ExamplePhaseListener();
		}
		return instance;
	}

	public void afterPhase(PhaseEvent event) {
		phaseId = event.getPhaseId();
		// Utils.debugRequestScope("after " + event.getPhaseId().getOrdinal());
		switch (event.getPhaseId().getOrdinal()) {
		case RESTORE_VIEW:
			afterRestoreView(event);
			break;
		case APPLY_REQUEST_VALUES:
			afterApplyRequestValues(event);
			break;
		case PROCESS_VALIDATIONS:
			afterProcessValidations(event);
			break;
		case UPDATE_MODEL_VALUES:
			afterUpdateModelValues(event);
			break;
		case INVOKE_APPLICATION:
			afterInvokeApplication(event);
			break;
		case RENDER_RESPONSE:
			afterRenderResponse(event);
			break;
		}
	}

	public void beforePhase(PhaseEvent event) {
		phaseId = event.getPhaseId();
		// Utils.debugRequestScope("before" + event.getPhaseId().getOrdinal());
		switch (event.getPhaseId().getOrdinal()) {
		case RESTORE_VIEW:
			beforeRestoreView(event);
			break;
		case APPLY_REQUEST_VALUES:
			beforeApplyRequestValues(event);
			break;
		case PROCESS_VALIDATIONS:
			beforeProcessValidations(event);
			break;
		case UPDATE_MODEL_VALUES:
			beforeUpdateModelValues(event);
			break;
		case INVOKE_APPLICATION:
			beforeInvokeApplication(event);
			break;
		case RENDER_RESPONSE:
			beforeRenderResponse(event);
			break;
		}
	}

	protected void beforeRestoreView(PhaseEvent event) {
		print(event, "Starting");
	}

	protected void afterRestoreView(PhaseEvent event) {
		print(event, "Finished");
	}

	protected void beforeApplyRequestValues(PhaseEvent event) {
		print(event, "Starting");
	}

	protected void afterApplyRequestValues(PhaseEvent event) {
		print(event, "Finished");
	}

	protected void beforeProcessValidations(PhaseEvent event) {
		print(event, "Starting");
	}

	protected void afterProcessValidations(PhaseEvent event) {
		print(event, "Finished");
	}

	protected void beforeInvokeApplication(PhaseEvent event) {
		print(event, "Starting");
	}

	protected void afterUpdateModelValues(PhaseEvent event) {
		print(event, "Finished");
	}

	protected void beforeUpdateModelValues(PhaseEvent event) {
		print(event, "Starting");
	}

	protected void afterInvokeApplication(PhaseEvent event) {
		print(event, "Finished");
	}

	protected void beforeRenderResponse(PhaseEvent event) {
		print(event, "Starting");
	}

	protected void afterRenderResponse(PhaseEvent event) {
		print(event, "Finished");
	}

	public boolean isRestoreViewPhase() {
		return (null != phaseId && phaseId.equals(PhaseId.RESTORE_VIEW));
	}

	public boolean isApplyRequestValuesPhase() {
		return (null != phaseId && phaseId.equals(PhaseId.APPLY_REQUEST_VALUES));
	}

	public boolean isProcessValidationsPhase() {
		return (null != phaseId && phaseId.equals(PhaseId.PROCESS_VALIDATIONS));
	}

	public boolean isUpdateModelValuesPhase() {
		return (null != phaseId && phaseId.equals(PhaseId.UPDATE_MODEL_VALUES));
	}

	public boolean isInvokeApplicationPhase() {
		return (null != phaseId && phaseId.equals(PhaseId.INVOKE_APPLICATION));
	}

	public boolean isRenderResponsePhase() {
		return (null != phaseId && phaseId.equals(PhaseId.RENDER_RESPONSE));
	}

	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	protected void print(PhaseEvent event, String message) {
		String pageName = "";
		UIViewRoot root = FacesContextEx.getCurrentInstance().getViewRoot();
		if (root instanceof UIViewRootEx) {
			UIViewRootEx rootEx = (UIViewRootEx) root;
			pageName = rootEx.getPageName();
		}
		String phaseName = event.getPhaseId().toString();
		System.out.println(pageName + " - " + phaseName + " - " + message);
	}

}
