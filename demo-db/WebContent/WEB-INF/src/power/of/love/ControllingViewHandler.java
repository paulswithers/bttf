package power.of.love;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

import com.ibm.xsp.application.ViewHandlerExImpl;
import com.ibm.xsp.component.UIViewRootEx;

public class ControllingViewHandler extends ViewHandlerExImpl {

	public ControllingViewHandler(ViewHandler arg0) {
		super(arg0);
	}

	@Override
	public UIViewRoot createView(FacesContext arg0, String arg1) {
		UIViewRootEx root = null;
		System.out.println("Creating component tree");
		root = (UIViewRootEx) super.createView(arg0, arg1);
		System.out.println("Created component tree");
		return root;
	}

}
